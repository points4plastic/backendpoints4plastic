package org.p4p.web;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by kgei2801 on 24/11/2016.
 */
public class StatsRepresentation {

    @JsonProperty("co2Total")
    private Integer co2Total;

    @JsonProperty("type")
    private List<TypeRepresentation> type;

    @JsonProperty("shoppings")
    private List<ShoppingRepresentation> shoppings;

    public Integer getCo2Total() {
        return co2Total;
    }

    public void setCo2Total(Integer co2Total) {
        this.co2Total = co2Total;
    }

    public List<TypeRepresentation> getType() {
        return type;
    }

    public void setType(List<TypeRepresentation> type) {
        this.type = type;
    }

    public List<ShoppingRepresentation> getShoppings() {
        return shoppings;
    }

    public void setShoppings(List<ShoppingRepresentation> shoppings) {
        this.shoppings = shoppings;
    }
}
