package org.p4p.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kgei2801 on 24/11/2016.
 */
@RestController("stats")
public class StatsResource {

    @RequestMapping(method = RequestMethod.DELETE)
    public void addStats(@RequestBody String bonid) {

    }

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public StatsRepresentation getStats() {
        StatsRepresentation statsRepresentation = new StatsRepresentation();

        return statsRepresentation;
    }
}
