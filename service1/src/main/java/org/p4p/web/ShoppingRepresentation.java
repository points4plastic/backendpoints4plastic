package org.p4p.web;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kgei2801 on 24/11/2016.
 */
public class ShoppingRepresentation {

    @JsonProperty("id")
    private String id;

    @JsonProperty("points")
    private Integer points;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
}
