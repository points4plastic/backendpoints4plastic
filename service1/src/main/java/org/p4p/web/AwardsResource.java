package org.p4p.web;

import com.google.common.io.ByteStreams;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by kgei2801 on 24/11/2016.
 */
@RestController
@RequestMapping("awards")
public class AwardsResource {

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public HttpEntity<byte[]> getImage() throws IOException {

        byte[] image = ByteStreams.toByteArray(AwardsResource.class.getClassLoader().getResourceAsStream("dummy.jpeg"));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(image.length);

        return new HttpEntity<byte[]>(image, headers);
    }
}
