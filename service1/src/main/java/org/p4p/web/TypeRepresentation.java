package org.p4p.web;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kgei2801 on 24/11/2016.
 */
public class TypeRepresentation {

    @JsonProperty("type")
    private String type;

    @JsonProperty("value")
    private Integer value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
