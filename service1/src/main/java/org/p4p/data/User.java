package org.p4p.data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name = "User")
@Table(name = "user")
public class User {

    protected User(){}

    public User(String email) {
        this.email = email;
    }

    public User(String email, String lastname, String name) {
        this.email = email;
        this.lastname = lastname;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "name")
    private String name;

    @Column(name = "points",nullable = false)
    private Integer points=0;

    @Column(name = "created",nullable = false)
    private Timestamp created = new Timestamp(new Date().getTime());
    @Column(name = "lastlogin",nullable = false)
    private Timestamp lastlogin = new Timestamp(new Date().getTime());
    @Column(name = "active", nullable = false)
    private boolean active=true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(Timestamp lastlogin) {
        this.lastlogin = lastlogin;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
