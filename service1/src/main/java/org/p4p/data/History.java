package org.p4p.data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name = "History")
@Table(name = "history", uniqueConstraints = @UniqueConstraint(columnNames = {"storeid", "bonid"}))
public class History {

    protected History(){}

    public History(User userid, Long storeId, Long bonid, Timestamp created, Long points, String refundtype) {
        this.userid = userid;
        this.storeId = storeId;
        this.bonid = bonid;
        this.created = created;
        this.points = points;
        this.refundtype = refundtype;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "userid", nullable = false)
    private User userid;

    @Column(name = "storeid", nullable = false)
    private Long storeId = 0L;

    @Column(name = "bonid", nullable = false)
    private Long bonid;

    @Column(name = "created", nullable = false)
    private Timestamp created = new Timestamp(new Date().getTime());

    @Column(name = "points", nullable = false)
    private Long points;

    @Column(name = "refundtype", nullable = false)
    private String refundtype;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getBonid() {
        return bonid;
    }

    public void setBonid(Long bonid) {
        this.bonid = bonid;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public String getRefundType() {
        return refundtype;
    }

    public void setRefundType(String refundtype) {
        this.refundtype = refundtype;
    }
}
