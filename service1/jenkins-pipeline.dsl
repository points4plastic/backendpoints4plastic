node {
stage 'build'
openshiftBuild(buildConfig: 'service1', showBuildLogs: 'true')
stage 'deploy integration'
openshiftVerifyDeployment(deploymentConfig: 'service1')
stage 'test integration'
sh 'curl -i -s http://service1-points4plastic.apps.ose.sademo.de/ | head -1 |grep 200; exit $?'
stage 'promote to uat'
openshiftTag(alias: 'false', apiURL: '', authToken: '', destStream: 'service1', destTag: 'master', destinationAuthToken: '', destinationNamespace: 'points4plastic-uat', namespace: 'points4plastic', srcStream: 'service1', srcTag: 'latest', verbose: 'false')
openshiftTag(alias: 'false', apiURL: '', authToken: '', destStream: 'service1', destTag: 'latest', destinationAuthToken: '', destinationNamespace: 'points4plastic-uat', namespace: 'points4plastic-uat', srcStream: 'service1', srcTag: 'master', verbose: 'false')
stage 'deploy uat'
openshiftVerifyDeployment(namespace: 'points4plastic-uat', deploymentConfig: 'service1')
openshiftScale(namespace: 'points4plastic-uat', deploymentConfig: 'service1',replicaCount: '2')
stage 'test uat'
sh 'curl -i -s http://service1-points4plastic-uat.apps.ose.sademo.de/ | head -1 |grep 200; exit $?'
stage 'promote to production'
openshiftTag(alias: 'false', apiURL: '', authToken: '', destStream: 'service1', destTag: 'master', destinationAuthToken: '', destinationNamespace: 'points4plastic-production', namespace: 'points4plastic-uat', srcStream: 'service1', srcTag: 'master', verbose: 'false')
openshiftTag(alias: 'false', apiURL: '', authToken: '', destStream: 'service1', destTag: 'latest', destinationAuthToken: '', destinationNamespace: 'points4plastic-production', namespace: 'points4plastic-production', srcStream: 'service1', srcTag: 'master', verbose: 'false')
stage 'deploy production'
openshiftVerifyDeployment(namespace: 'points4plastic-production', deploymentConfig: 'service1')
openshiftScale(namespace: 'points4plastic-production', deploymentConfig: 'service1', replicaCount: '2')
}
