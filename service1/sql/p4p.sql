insert into user (id, active, created, email, lastname, name, points) values (1, 1, '2016-11-24 16:01:00', 'joedoe@test.local', 'Doe', 'Joe', 0);
insert into user (id, active, created, email, lastname, name, points) values (2, 1, '2016-11-24 16:01:03', 'joshmo@test.local', 'Shmo', 'Jo', 0);

insert into history (id, userid, bonid, created, points, refundtype, storeid) values (1, 1, 123, '2016-11-24', 20, 'Plastic', 1);
insert into history (id, userid, bonid, created, points, refundtype, storeid) values (2, 2, 125, '2016-11-24', 30, 'Without packaging', 1);
insert into history (id, userid, bonid, created, points, refundtype, storeid) values (3, 1, 133, '2016-11-24', 10, 'Pfand', 1);

update user set points=30 where id=1;
update user set points=30 where id=2;
